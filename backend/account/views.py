from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import BloggerRegistrationSerializer, BrandRegistrationSerializer

User = get_user_model()


class DefaultRegistration(APIView):
    """ Общий класс регистрации блогера и бренда """
    permission_classes = (AllowAny,)
    serializer_class = None

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class BloggerRegistration(DefaultRegistration):
    """ Регистрация блогера """
    serializer_class = BloggerRegistrationSerializer


class BrandRegistration(DefaultRegistration):
    """ Регистрация бренда """
    serializer_class = BrandRegistrationSerializer


class LoginView(ObtainAuthToken):
    """ Авторизация пользователя """

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, create = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
        })


class LogoutView(APIView):

    @staticmethod
    def get(request):
        print(1)
        print(request.user)
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


class HelloView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request):
        content = {'message': 'Hello, World!'}
        return Response(content)

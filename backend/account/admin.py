from django.contrib import admin
from .models import Brand, Blogger, CustomUser
from django.contrib.auth.admin import UserAdmin


# class UserCreationForm(forms.ModelForm):
#     name = forms.CharField(label='Имя')
#     password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput)
#     password2 = forms.CharField(label='Подтверждение пароля', widget=forms.PasswordInput)
#
#     class Meta:
#         model = Profile
#         fields = ('first_name', 'last_name', 'email', 'phone', 'type_user')
#
#     def clean_password2(self):
#         password1 = self.cleaned_data.get("password1")
#         password2 = self.cleaned_data.get("password2")
#         if password1 and password2 and password1 != password2:
#             raise forms.ValidationError("Passwords don't match")
#         return password2
#
#     def save(self, commit=True):
#         user = super().save(commit=False)
#         user.set_password(self.cleaned_data["password1"])
#         if commit:
#             user.save()
#         return user


@admin.register(CustomUser)
class AdminUser(admin.ModelAdmin):
    list_display = ('email', 'created_at', 'is_active')
    list_editable = ('is_active',)

    def get_queryset(self, request):
        return super(AdminUser, self).get_queryset(request).filter(is_superuser=True)


@admin.register(Brand)
class BrandUser(admin.ModelAdmin):
    list_display = ('name', 'email', 'is_active')
    list_editable = ('is_active',)


@admin.register(Blogger)
class BloggerUser(admin.ModelAdmin):
    list_display = ('fio', 'email', 'instagram', 'date_of_birth', 'gender', 'is_active')
    list_editable = ('is_active',)

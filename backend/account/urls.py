from django.urls import path

from .views import LoginView, LogoutView, BloggerRegistration, BrandRegistration, HelloView

app_name = 'account'

urlpatterns = [
    path('registration/blogger/', BloggerRegistration.as_view(), name='register-blogger'),
    path('registration/brand/', BrandRegistration.as_view(), name='register-brand'),
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('hello', HelloView.as_view(), name='hello'),

]

from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _


class CustomUserManager(BaseUserManager):
    """
    Кастом менеджер для регистрация пользователей по email,
    который является уникальным
    """
    def create_user(self, email: str, password: str, **extra_fields) -> object:
        """
        Регитсрация пользователя по email и password
        """
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email: str, password: str, **extra_fields) -> object:
        """
        Регитсрация суперпользователя по email и password
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)


class BloggerManager(BaseUserManager):

    def create_blogger(self, fio: str, email: str, date_of_birth: str,
                       instagram: str, gender: str,
                       password=None, **extra_fields) -> object:
        """
        Регитсрация пользователя БЛОГГЕР по email и password
        """

        if not email:
            raise ValueError(_('Users must have an email address.'))
        email = self.normalize_email(email)
        blogger = self.model(email=email, fio=fio, instagram=instagram,
                             date_of_birth=date_of_birth, gender=gender)
        blogger.set_password(password)
        blogger.is_active = False
        blogger.save()
        return blogger


class BrandManager(BaseUserManager):

    def create_brand(self, name: str, email: str, password=None, **extra_fields) -> object:
        """
        Регитсрация пользователя БРЕНД по email и password
        """
        if not email:
            raise ValueError(_('Users must have an email address.'))
        email = self.normalize_email(email)
        brand = self.model(email=email, name=name)
        brand.set_password(password)
        brand.is_active = False
        brand.save()
        return brand

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
import os

User = get_user_model()


class Command(BaseCommand):
    """ Проверка на наличие учетной записи супер пользователя и устновки если она отсуствует  """

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            User.objects.create_superuser(
                os.environ.get("SUPERUSER_EMAIL_DEFAULT"),
                os.environ.get("SUPERUSER_PASSWORD_DEFAULT"))

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class RegistrationBloggerTest(APITestCase):

    def test_registration_blogger(self):
        data = {
            "email": "testttt@mail.ru",
            "fio": "Test test",
            "instagram": "@insta",
            "date_of_birth": "1987-10-15",
            "gender": "M",
            "password": "123123123"
        }
        url = reverse('account:register-blogger')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_brand(self):
        data = {
            "email": "testttt@mail.ru",
            "name": "Test test",
            "password": "123123123"
        }
        url = reverse('account:register-brand')
        response = self.client.post(url, data)
        print(response.status_code)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

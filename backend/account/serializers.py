from django.contrib.auth import authenticate

from .models import Blogger, Brand
from django.utils.translation import ugettext_lazy as _
from rest_framework.serializers import ModelSerializer, \
    CharField, EmailField, DateField, ValidationError


class DefaultRegistrationSerializer(ModelSerializer):
    password = CharField(
        max_length=20,
        min_length=8,
        write_only=True
    )
    # password2 = CharField(
    #     max_length=20,
    #     min_length=8,
    #     write_only=True,
    #     required=True)
    email = EmailField(max_length=20)

    def validate(self, attrs):
        # if attrs['password'] != attrs['password2']:
        #     raise ValidationError({"password": "Password fields didn't match."})
        email = attrs.get('email', '')
        instagram = attrs.get('instagram', '')
        name = attrs.get('name', '')
        if Blogger.objects.filter(email=email).exists():
            raise ValidationError({'email': _('Email is already in use')})
        elif Blogger.objects.filter(instagram=instagram).exists():
            raise ValidationError({'instagram': _('Instagram is already in use')})
        elif Brand.objects.filter(email=email).exists():
            raise ValidationError({'email': _('Email is already in use')})
        elif Brand.objects.filter(name=name).exists():
            raise ValidationError({'name': _('Name is already in use')})
        return super().validate(attrs)


class BloggerRegistrationSerializer(DefaultRegistrationSerializer):
    fio = CharField(max_length=20, min_length=3)
    instagram = CharField(max_length=50)
    gender = CharField(max_length=8)
    date_of_birth = DateField()

    class Meta:
        model = Blogger
        fields = ['fio', 'instagram', 'email', 'date_of_birth', 'gender', 'password']

    def create(self, validated_data):
        return Blogger.objects.create_blogger(**validated_data)


class BrandRegistrationSerializer(DefaultRegistrationSerializer):
    name = CharField(max_length=20)

    class Meta:
        model = Brand
        fields = ['name', 'email', 'password']

    def create(self, validated_data):
        return Brand.objects.create_brand(**validated_data)

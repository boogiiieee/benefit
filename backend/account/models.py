from django.contrib.auth.models import AbstractUser, PermissionsMixin, AbstractBaseUser
from django.db import models
from settings import settings
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from .managers import CustomUserManager, BloggerManager, BrandManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name=_('Email address'), max_length=20, unique=True, db_index=True)
    created_at = models.DateTimeField(verbose_name=_('created'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated_at'), auto_now=True)
    is_staff = models.BooleanField(verbose_name=_('staff status'), default=False)
    is_superuser = models.BooleanField(verbose_name=_('staff status'), default=False)
    is_active = models.BooleanField(verbose_name=_('active'), default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = _('Admin')
        verbose_name_plural = _('Admins')

    def __str__(self):
        return self.email


class Blogger(CustomUser, PermissionsMixin):
    """ Описание модели БЛОГЕРЫ """
    GENDER_CHOICES = [
        (_('M'), _('Man')),
        (_('F'), _('Female')),
    ]

    REQUIRED_FIELDS = ['fio', 'email', 'instagram', 'date_of_birth', 'gender']

    objects = BloggerManager()

    fio = models.CharField(verbose_name=_('fullname blogger'), max_length=50, db_index=True)
    instagram = models.CharField(verbose_name=_('instagram blogger'), max_length=50, unique=True, db_index=True)
    date_of_birth = models.DateField(verbose_name=_('date of birth'))
    gender = models.CharField(verbose_name=_('gender'), choices=GENDER_CHOICES, max_length=8)

    class Meta:
        verbose_name = _('Blogger')
        verbose_name_plural = _('Bloggers')


class Brand(CustomUser, PermissionsMixin):
    """ Описание модели БРЕНД """

    REQUIRED_FIELDS = ['name']

    objects = BrandManager()

    name = models.CharField(verbose_name=_('name brand'), max_length=20, db_index=True)

    class Meta:
        verbose_name = _('Brand')
        verbose_name_plural = _('Brands')


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
@receiver(post_save, sender=Brand)
@receiver(post_save, sender=Blogger)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """ Генерация токена при создании пользователя """
    if created:
        Token.objects.create(user=instance)

